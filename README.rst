Lispism
=======

A library implementing features of Lisp in Python.

Currently, the only main feature implemented is Dynamically Scoped Variables.

For Users
---------

Dynamically Scoped Variables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These are variables in thread-local storage, whose values can only be
changed using a context-manager which restores the older value on exit.
They "live" in a Dynamic Namespace -- an object which allows access to
their current value, and provides the ``let()`` context-manager to change
them.

This is all best explained by an example::

    from lispism import D

    with D.let(a=1, b=2):
        print (D.a)          # prints 1
        with D.let(a=3, b=4):
            print(D.a)       # prints 3
        print (D.a)          # prints 1
    D.a                      # raises NameError

``D`` is the default Dynamic Namespace. ``DN`` is a namespace which shares
the same variables, but gives ``None`` when an undefined variable is referenced
(instead of raising ``NameError``).

It is also possible, in this way, to modify attributes on an existing objects::

    with D.let(obj=Cls()):
        # assume Cls() initializes attribute a to 5
	print (D.obj.a)      # prints 5
        with D.let(obj__a=9):
	    print (D.obj.a)  # prints 9
	print (D.obj.a)      # prints 5

Dynamic variables are protected against direct changes; you can only
change them using ``let()`` (well, there are a few caveats to that,
but that is the general idea).

Python Compatibility
~~~~~~~~~~~~~~~~~~~~

We use six_ to provide Python 2 and Python 3 compatibility in one source.
Where we need to choose which to support better, Python 3 takes precedence.

.. _six: https://pypi.python.org/pypi/six

For Developers
--------------

Code structure
~~~~~~~~~~~~~~

There are two main source files: "frozen.py" takes care of making objects
read-only, and "dynvar.py" defines the objects which implement dynamic
variables -- dynamic dicts and dynamic namespaces. "__init__.py" defines
the special names ``D`` and ``DN``.

Tests are in a separate folder.

TODO
~~~~
These are plans we have for the future:

- Take care of ``__dict__`` access
- Introduce `MacroPy`_ as an optional dependency, and use it to:

  + Support better sub-object assignment in ``let()``
  + Prevent ``yield`` under ``with let()``.

.. _MacroPy: https://pypi.python.org/pypi/MacroPy
