from .dynvar import DynamicNamespace, DynamicObjDict

_dict = DynamicObjDict()

D = DynamicNamespace(dyndict=_dict)
DN = DynamicNamespace(dyndict=_dict, default=None)
