# encoding: utf8
"""
Dynamic variables are implemented as members of a Dynamic Namespace,
which is a namespace managed in a Dynamic Dictionary.

Intended use (assuming D is a DynamicNamespace):

    with D.let(name=value):
        ...
        # code here can use D.name; code called from here can also
        # use D.name.

Once a name has been defined, its attributes can be temporarily changed by:

    with D.let(name__attr=sub_value):
        ...
        # code here gets sub_value from D.name.attr
    ...
    # code here does not see the assignment to name.attr, only to name

This file defines:
- DynamicDict:: 
    a basic Dynamic Dictionary

- DynamicObjDict::
    a Dynamic Dictionary with support for dynamic assignment to attributes
    of objects.

- DynamicNamespace::
    a wrapper around Dynamic Dictionaries to allow using attribute-access
    syntax (`D.var` instead of `D['var']`).
"""
from contextlib import contextmanager
from copy import deepcopy
from threading import local

import six

from .frozen import freeze

class DynamicDict(local):
    """
    A thread-local stack of dictionaries, providing a dictionary interface,
    where lookup returns the top-most occurence of the key in the stack.
    """

    __sentinel = object()
    
    # Instance invariant: the stack is never empty
    
    def __init__(self, stack=None):
        self._stack = [{}] if stack is None else stack

    # Dictionary-stack interface
    
    @property
    def top(self):
        return self._stack[0]

    def flat(self):
        d = {}
        for l in reversed(self._stack):
            d.update(l)
        return d

    @contextmanager
    def newlevel(self):
        self._stack.insert(0, {})
        try:
            yield
        finally:
            self._stack.pop(0)

    @contextmanager
    def let(self, **kwargs):
        with self.newlevel():
            for key, value in kwargs.items():
                self[key] = value
            yield

    # Dictionary interface
    
    def __setitem__(self, key, value):
        self.top[key] = value

    def __getitem__(self, key):
        for level in self._stack:
            try:
                return level[key]
            except KeyError:
                pass
        raise KeyError(key)

    def __contains__(self, key):
        return any(key in level for level in self._stack)

    def __iter__(self):
        return iter(self.items())

    def __len__(self):
        return len(self.keys())

    def copy(self):
        # 2-level-deep copy
        other = self.__class__([d.copy() for d in self._stack])
        return other

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def setdefault(self, key, default):
        try:
            return self[key]
        except KeyError:
            return self.top.setdefault(key, default)        

    def update(self, *args, **kw):
        return self.top.update(*args, **kw)
        
    def keys(self):
        return set().union(*(level.keys() for level in self._stack))

    def items(self):
        return self.flat().items()

    def values(self):
        return self.flat().values()

    # __delitem__, clear, pop, popitem not supported because no
    # reasonable sematics can be given

class DynamicObjDict(DynamicDict):

    @contextmanager
    def let(self, **kwargs):
        """
        Supports both "let" and "letattr" via obj__attr syntax
        """
        with self.newlevel():
            for key, value in kwargs.items():
                attrs = key.split("__")
                if len(attrs)==1:
                    self[key] = value
                else:
                    key = attrs.pop(0)
                    if key in self.top:
                        # This is needed to support let(a__b=X, a__c=Y)
                        obj = self.top[key]
                    else:
                        obj = self[key] = deepcopy(self[key]) # fetch from deeper level
                    for attr in attrs[:-1]:
                        obj = getattr(obj, attr)
                    setattr(obj, attrs[-1], value)
            yield

class DynamicNamespace(object):
    __slots__ = ['__dyndict__', '__default__']
    __sentinel = object()
    
    def __init__(self, default=__sentinel, dyndict=None):
        if dyndict is None:
            dyndict = DynamicObjDict()
        self.__dyndict__ = dyndict
        self.__default__ = default

    def let(self, **kwargs):
        """
        Modifying interface, a context manager.
        Intended use (assuming D is an instance of this class):

            with D.let(name=value):
                ...
                # code here can use D.name

        Once a name has been defined, its attributes can be temporarily changed by:

            with D.let(name__attr=sub_value):
                ...
                # code here gets sub_value from D.name.attr
            ...
            # code here does not see the assignment to name.attr, only to name
        """
        return self.__dyndict__.let(**kwargs)

    def __getattr__(self, attr):
        try:
            value = self.__dyndict__[attr]
        except KeyError:
            if self.__default__ is self.__sentinel:
                raise NameError("Dynamic var '{}' is not defined".format(attr))
            return self.__default__
        else:
            return freeze(value)

    def __setattr__(self, attr, value):
        """This is mostly to generate friendlier error messages"""
        if attr in self.__slots__:
            object.__setattr__(self, attr, value)
        else:
            raise TypeError("To change dynamic variables in this namespace, use its `let()` method")
