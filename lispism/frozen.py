# encoding: utf8
# Python 3 source, Python 2 compatibility as best-effort
"""
doctoc dodoc
"""

import six

class FrozenMixin(object):
    """
    A mixin for classes to make them "frozen", that is, to make
    their instances act like immutables.

    The intention is not to make the instances really immutable --
    they will still not be usable as keys, and there is no intention
    for the immutability to make them "safe" in the sense of security.
    The intention is to achieve read-only in terms of software 
    engineering, under assumptions of "consenting adults": You can
    change it if you really insist, but you need to be very explicit
    about it.
    """
    def __getattribute__(self, attr):
        # todo: better handling of missing attributes (throw from non-frozen parent)
        value = super(FrozenMixin, self).__getattribute__(attr)
        if attr.startswith('__'):
            return value
        elif isinstance(value, six.types.BuiltinMethodType):
            return lambda *args, **kw: freeze(value(*args,**kw))
        return freeze(super(FrozenMixin, self).__getattribute__(attr))

    def __setattr__(self, attr, value):
        raise TypeError("This is a frozen instance, no setting attributes")

    def __delattr__(self, attr):
        raise TypeError("This is a frozen instance, no deleting attributes")

    def __getitem__(self, item):
        # todo: better error message for non-subscriptables
        return freeze(super(FrozenMixin, self).__getitem__(item))

    def __setitem__(self, item, value):
        raise TypeError("This is a frozen instance, no setting items")

    def __delitem__(self, item):
        raise TypeError("This is a frozen instance, no deleting items")

    def __iter__(self):
        return frozen_iterator(super(FrozenMixin, self).__iter__())

    def _raise_invalid_change(self):
        raise TypeError("This is a frozen instance, modifying operations verboten")

class FrozenUserMixin(FrozenMixin):
    def __init__(self, orig):
        object.__setattr__(self, '__orig', orig)
        
    def __getattr__(self, attr):
        orig = object.__getattribute__(self, '__orig')
        return freeze(getattr(orig, attr))
    

immutable_types = frozenset([
    int,
    bool,
    float,
    str,
    bytes,
    slice,
    type(None),
    type(Ellipsis),
    six.types.BuiltinMethodType,
    six.types.MethodType,
])

class frozen_tuple(FrozenMixin, tuple):
    """
    This class is used to freeze both tuples and lists
    """
    def __getattribute__(self, attr):
        """
        This method is only intended to translate AttributeError's
        for list methods into TypeError's
        """
        try:
            return super(frozen_tuple, self).__getattribute__(attr)
        except AttributeError:
            if hasattr([], attr):
                self._raise_invalid_change()
            else:
                raise

class frozen_frozenset(FrozenMixin, frozenset):
    def copy(self):
        return self.__class__(self)

class frozen_dict(FrozenMixin, dict):
    """
    Must override dict-changing methods
    """
    def clear(self):
        self._raise_invalid_change()

    def copy(self):
        return self.__class__(self)

    if six.PY2:
        def iteritems(self):
            return frozen_iterator(super(FrozenMixin, self).iteritems())

        def iterkeys(self):
            return frozen_iterator(super(FrozenMixin, self).iterkeys())

        def itervalues(self):
            return frozen_iterator(super(FrozenMixin, self).itervalues())

    def pop(self, *args):
        self._raise_invalid_change()

    def popitem(self, *args):
        self._raise_invalid_change()

    def setdefault(self, *args):
        self._raise_invalid_change()

    def update(self, *args, **kw):
        self._raise_invalid_change()

    if six.PY2:
        def _view_not_allowed(self):
            """
            Frozen instances are presented as immutable. Thus, a view makes no sense.
            Taking a view, if allowed, may lead to bugs -- as changing the values of
            frozen objects usually involves replacing the reference to them; meaning
            that views would be showing the un-updated values.
            """
            raise TypeError("Views over frozen instances are not allowed")
        
        def viewitems(self):
            self._view_not_allowed()

        def viewkeys(self):
            self._view_not_allowed()

        def viewvalues(self):
            self._view_not_allowed()            

    if six.PY3:
        # With Python 3 keys, values and items are iterators
        def items(self):
            return frozen_iterator(super(FrozenMixin, self).items())

        def keys(self):
            return frozen_iterator(super(FrozenMixin, self).keys())

        def values(self):
            return frozen_iterator(super(FrozenMixin, self).values())

def frozen_iterator(iterator):
    for item in iterator:
        yield freeze(item)

frozen_transformers = {
    frozenset : frozen_frozenset,
    set : frozen_frozenset,
    tuple: frozen_tuple,
    list: frozen_tuple,
    bytearray: bytes,
    dict: frozen_dict,
}

def freeze(value):
    typ = type(value)
    if issubclass(typ, FrozenMixin):
        return value
    if typ in immutable_types:
        return value
    else:
        try:
            ftype = frozen_transformers[typ]
        except KeyError:
            ftype_name = 'frozen_' + typ.__name__
            bases = FrozenUserMixin, typ
            ftype = type(ftype_name, bases, {})
            frozen_transformers[typ] = ftype
        return ftype(value)
