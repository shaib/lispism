from unittest import TestCase

from lispism import D, DN

class TestD(TestCase):
    """
    Test the public "lispism.D" interface
    """
    def test_let_simple(self):
        with D.let(a=1, b=2):
            self.assertEqual(D.a, 1)
            self.assertEqual(D.b, 2)
            with D.let(a=3, b=4):
                self.assertEqual(D.a, 3)
                self.assertEqual(D.b, 4)
            self.assertEqual(D.a, 1)
            self.assertEqual(D.b, 2)
        with self.assertRaises(NameError):
            D.a

    def test_let_frozen(self):
        with D.let(a=[2], b=[[6]], c={4}):
            self.assertEqual(D.a, (2,))
            self.assertEqual(D.b[0], (6,))
            self.assertEqual(D.c, {4})
            with self.assertRaises(AttributeError):
                D.c.add(23)

    def test_let_frozen_userclass(self):
        class Cls(object):
            def __init__(self):
                self.a = 5
                self.b = [6]
                self.c = {'d':4}
            def reader(self):
                return self.b
            def writer(self, val):
                self.b.append(val)

        obj = Cls()
        # Just making sure the test class is as planned
        self.assertEqual(obj.reader(), [6])
        obj.writer(7)
        self.assertEqual(obj.reader(), [6,7])
        # Now test it under 'let'
        with D.let(obj=Cls()):
            # Direct attribute access
            self.assertEqual(D.obj.a, 5)
            self.assertEqual(D.obj.b, (6,))
            self.assertEqual(D.obj.c, {'d':4})
            with self.assertRaises(TypeError):
                D.obj.a = 6
            with self.assertRaises(TypeError):
                del D.obj.c['d']
            with self.assertRaises(TypeError):
                del D.obj.b
            # Access through methods
            self.assertEqual(D.obj.reader(), (6,))
            with self.assertRaises(TypeError):
                D.obj.writer(8)
            # Let-attr
            with D.let(obj__a=9):
                self.assertEqual(D.obj.a, 9)
            self.assertEqual(D.obj.a, 5)

    def test_namespace_closed(self):
        with self.assertRaises(TypeError):
            D.x = 6

    def test_function_call(self):
        "Verify dynamic assigments become accessible in functions"
        def func():
            return D.x

        # No assignment of x
        self.assertRaises(NameError, func)

        with D.let(x=7):
            # Assignment, first level
            self.assertEqual(func(), 7)
            with D.let(x=9):
                # Assignment, second level
                self.assertEqual(func(), 9)
            # Assignment, first level
            self.assertEqual(func(), 7)
        # No assignment of x
        self.assertRaises(NameError, func)

class TestDN(TestCase):
    """
    Test the public `lispism.DN` interface which accesses the same namespace
    as `lispism.D` but returns `None` for undefined vars instead of raising
    a `NameError` when undefined variables are accessed.
    """
    def test_let_mixed(self):
        with D.let(a=1, b=2):
            self.assertEqual(DN.a, 1)
            self.assertEqual(DN.b, 2)
            with DN.let(a=3, b=4):
                self.assertEqual(D.a, 3)
                self.assertEqual(D.b, 4)
            self.assertEqual(D.a, 1)
            self.assertEqual(D.b, 2)
        with self.assertRaises(NameError):
            D.a
        self.assertIsNone(DN.a)

    def test_let_frozen(self):
        with DN.let(a=[2], b=[[6]], c={4}):
            self.assertEqual(DN.a, (2,))
            self.assertEqual(DN.b[0], (6,))
            self.assertEqual(DN.c, {4})
            with self.assertRaises(AttributeError):
                DN.c.add(23)

    def test_let_frozen_userclass(self):
        class Cls(object):
            def __init__(self):
                self.a = 5
                self.b = [6]
                self.c = {'d':4}
            def reader(self):
                return self.b
            def writer(self, val):
                self.b.append(val)

        obj = Cls()
        # Just making sure the test class is as planned
        self.assertEqual(obj.reader(), [6])
        obj.writer(7)
        self.assertEqual(obj.reader(), [6,7])
        # Now test it under 'let'
        with DN.let(obj=Cls()):
            # Direct attribute access
            self.assertEqual(DN.obj.a, 5)
            self.assertEqual(DN.obj.b, (6,))
            self.assertEqual(DN.obj.c, {'d':4})
            with self.assertRaises(TypeError):
                DN.obj.a = 6
            with self.assertRaises(TypeError):
                del DN.obj.c['d']
            with self.assertRaises(TypeError):
                del DN.obj.b
            # Access through methods
            self.assertEqual(DN.obj.reader(), (6,))
            with self.assertRaises(TypeError):
                DN.obj.writer(8)
            # Let-attr
            with DN.let(obj__a=9):
                self.assertEqual(DN.obj.a, 9)
            self.assertEqual(DN.obj.a, 5)

    def test_namespace_closed(self):
        with self.assertRaises(TypeError):
            DN.x = 6

    def test_function_call(self):
        "Verify dynamic assigments become accessible in functions"
        def func():
            return DN.x

        # No assignment of x
        self.assertIsNone(func())

        with DN.let(x=7):
            # Assignment, first level
            self.assertEqual(func(), 7)
            with D.let(x=9):
                # Assignment, second level
                self.assertEqual(func(), 9)
            # Assignment, first level
            self.assertEqual(func(), 7)
        # No assignment of x
        self.assertIsNone(func())
