from unittest import TestCase, skip
import six

from lispism.frozen import freeze, FrozenMixin

class TestFreeze(TestCase):

    def assertFrozenList(self, fl):
        self.assertEqual(len(fl), 3)
        self.assertEqual(fl.count(1),1)
        self.assertEqual(fl.index(2),1)
        with self.assertRaises(TypeError):
            fl[2] = 17
        list_methods = ['append',
                        'extend',
                        'insert',
                        'pop',
                        'remove',
                        'reverse',
                        'sort',
        ]
        if six.PY3:
            list_methods += ['clear',
                             'copy', # verbotten because it unfreezes elements
            ]
        for method in list_methods:
            with self.assertRaises(TypeError):
                getattr(fl, method)
        with self.assertRaises(AttributeError):
            getattr(fl, 'no_such_method')

    def assertFrozenSet(self, fs):
        self.assertEqual(len(fs), 3)
        self.assertTrue(fs.issuperset(set([1,2])))
        #self.assertTrue(fl.issuperset(set([1,2])))
        #self.assertEqual(fs.index(2),1)
        with self.assertRaises(AttributeError):
            fs.add(17)
        self.assertIsInstance(fs.copy(), FrozenMixin)
        for method in [ 'difference',
                        'intersection',
                        'symmetric_difference',
                        'union']:
            fresult = getattr(fs, method)({2})
            self.assertIsInstance(fresult, FrozenMixin)
            self.assertEqual(fresult, getattr({1,2,3}, method)({2}))

    def assertFrozenDict(self, fd):
        self.assertEqual(len(fd), 2)
        self.assertEqual(fd['a'], 1)
        self.assertEqual(fd[2], 'b')
        self.assertIsNone(fd.get('c'))
        self.assertIsInstance(fd.copy(), FrozenMixin)
        with self.assertRaises(TypeError):
            fd[3] = 3
        with self.assertRaises(TypeError):
            fd.update(x=3)
        with self.assertRaises(TypeError):
            fd.setdefault('a', 6)

    def test_freeze_simple_list(self):
        l = [1,2,3]
        fl = freeze(l)
        self.assertFalse(l is fl)
        self.assertEqual(fl, tuple(l))
        self.assertFrozenList(fl)
        
    def test_freeze_simple_tuple(self):
        t = (1,2,3)
        ft = freeze(t)
        self.assertFalse(t is ft)
        self.assertEqual(len(ft), 3)
        self.assertEqual(ft, t)
        self.assertEqual(ft.count(1),1)
        self.assertEqual(ft.index(2),1)
        with self.assertRaises(TypeError):
            ft[2] = 17

    def test_list_in_tuple(self):
        t = (1,2,[1,2,3],4)
        ft = freeze(t)
        self.assertFrozenList(ft[2])
        self.assertTrue(any(isinstance(item,FrozenMixin) for item in ft))
        for item in ft:
            try:
                item[0]
            except TypeError:
                pass
            else:
                self.assertFrozenList(item)

    def test_list_operators(self):
        l = [1,2,3]
        fl = freeze(l)
        with self.assertRaises(TypeError):
            fl += [4]

    def test_simple_frozenset(self):
        s = frozenset([1,2,3])
        fs = freeze(s)
        self.assertFrozenSet(fs)
                        
    def test_simple_frozendict(self):
        d = {"a":1, 2:"b"}
        fd = freeze(d)
        self.assertFrozenDict(fd)

    def assertFrozenObject(self, fu):
        self.assertFrozenList(fu.tuple)
        self.assertFrozenList(fu.list)
        self.assertFrozenDict(fu.dict)
        self.assertFrozenSet(fu.set)
        self.assertFrozenSet(fu.frozenset)
        with self.assertRaises(TypeError):
            del fu.set
        with self.assertRaises(TypeError):
            fu.list = [2,3,4]

    def test_simple_userobj(self):
        class U(object): pass
        u = U()
        u.tuple = (1,2,3)
        u.list = [1,2,3]
        u.dict = {"a":1, 2:"b"}
        u.set = {1, 2, 3}
        u.frozenset = frozenset(u.list)
        u.int = 6
        fu = freeze(u)
        self.assertFrozenObject(fu)
        with self.assertRaises(TypeError):
            fu.int += 7

    def test_userobj_with_descriptors(self):
        class Descriptor(object):
            def __init__(self, attr):
                self.attr = attr
            def __get__(self, inst, instcls):
                return getattr(inst, self.attr)
            def __set__(self, inst, value):
                return setattr(inst, self.attr, value)
            def __delete__(self, inst):
                return delattr(inst, self.attr)

        class U(object):
            tuple = Descriptor('tuple_')
            list = Descriptor('list_')
            dict = Descriptor('dict_')
            set = Descriptor('set_')
            frozenset = Descriptor('frozenset_')
        u = U()
        u.tuple = (1,2,3)
        u.list = [1,2,3]
        u.dict = {"a":1, 2:"b"}
        u.set = {1, 2, 3}
        u.frozenset = frozenset(u.list)
        fu = freeze(u)
        self.assertFrozenObject(fu)

    @skip("TODO: Special descriptor handling needed.")
    def test_userobj_with_dict_manipulating_descriptors(self):
        class Descriptor(object):
            def __init__(self, attr):
                self.attr = attr
            def __get__(self, inst, instcls):
                return inst.__dict__[self.attr]
            def __set__(self, inst, value):
                inst.__dict__[self.attr] = value
            def __delete__(self, inst):
                del inst.__dict__[self.attr]

        class U(object):
            tuple = Descriptor('tuple_')
            list = Descriptor('list_')
            dict = Descriptor('dict_')
            set = Descriptor('set_')
            frozenset = Descriptor('frozenset_')
        u = U()
        u.tuple = (1,2,3)
        u.list = [1,2,3]
        u.dict = {"a":1, 2:"b"}
        u.set = {1, 2, 3}
        u.frozenset = frozenset(u.list)
        fu = freeze(u)
        self.assertFrozenObject(fu)

    def test_userobj_with_methods(self):
        class U(object):
            def read(self):
                return self.list
            def write(self):
                self.dict['d'] = 17

        u = U()
        u.list = [1,2,3]
        u.dict = {"a":1, 2:"b"}
        fu = freeze(u)
        self.assertFrozenList(fu.read())
        with self.assertRaises(TypeError):
            fu.write()

    def test_userobj_with_operators(self):
        class U(object):
            def __init__(self):
                self.list = [1,2,3]
                self.dict = {"a":1, 2:"b"}
            def __pos__(self):
                return self.list
            def __iadd__(self, value):
                self.dict['a'] += value
                return self

        u = U()
        fu = freeze(u)
        self.assertFrozenList(+fu)
        with six.assertRaisesRegex(self, TypeError, ".*frozen instance.*"):
            try:
                fu += 5
            except:
                self.assertEqual(fu.dict['a'], 1)
                raise

    def test_iterators_iterable(self):
        class U(object): pass
        u = U()
        u.tuple = (1,2,3)
        u.list = [1,2,3]
        u.dict = {"a":1, 2:"b"}
        u.set = {1, 2, 3}
        u.frozenset = frozenset(u.list)
        fu = freeze(u)
        for fi, i in zip(fu.tuple, u.tuple):
            self.assertEqual(i, fi)
            t = True
        for fi, i in zip(iter(fu.tuple), u.tuple):
            self.assertEqual(i, fi)
            ti = True
        for fi, i in zip(iter(fu.list), u.list):
            self.assertEqual(i, fi)
            l = True
        for fi in fu.set:
            self.assertIn(fi, fu.set)
            self.assertIn(fi, u.set)
            s = True
        for fi in fu.frozenset:
            self.assertIn(fi, fu.frozenset)
            self.assertIn(fi, u.frozenset)
            f = True
        for fi in fu.dict:
            self.assertEqual(fu.dict[fi], u.dict[fi])
            d = True
        self.assertTrue(d and f and s and l and ti and t)

    def test_iterators_frozen(self):
        fl = freeze([[1],[2],[3]])
        for fi in iter(fl):
            self.assertIsInstance(fi, FrozenMixin)
            # TODO: add tests for iterating other containers

    def test_no_redundant_freezing(self):
        class U(object):
            def __init__(self):
                self.a = [1]
            def unfrozen(self):
                return [1]
            def frozen_contents(self):
                return {self.a}
        fu = freeze(U())
        self.assertEqual(fu.unfrozen(), [1])
        self.assertEqual(fu.frozen_contents(), {(1,)})
